import React from 'react';
import {connect} from 'react-redux';
import {Route, Switch, withRouter} from 'react-router-dom';
import Events from './containers/Events/Events';
import AddEvent from './containers/AddEvent/AddEvent';


const Routes = () => {
    return (
        <Switch>
            <Route path="/" exact component={Events}/>
            <Route path="/add" exact component={AddEvent}/>
        </Switch>
    )
};

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
};

export default withRouter(connect(mapStateToProps)(Routes));