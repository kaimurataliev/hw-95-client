import axios from '../../axios';

export const FETCH_EVENTS = 'FETCH_EVENTS';
export const ADD_EVENT_SUCCESS = 'ADD_EVENT_SUCCESS';
export const ADD_EVENT = 'ADD_EVENT';

export const addEventSuccess = (data) =>  {
    return {type: ADD_EVENT_SUCCESS, data}
};

export const addEvent = (event) => {
    return {type: ADD_EVENT, event}
};
export const fetchEvents = (data) => {
    return {type: FETCH_EVENTS, data}
};

export const fetchAllEvents = () => {
    return axios.get('/events')
};