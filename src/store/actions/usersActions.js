export const LOGIN_FACEBOOK_SUCCESS = 'LOGIN_FACEBOOK_SUCCESS';
export const REGISTER_USER ='REGISTER_USER';
export const REGISTER_FACEBOOK = 'REGISTER_FACEBOOK';
export const LOGOUT_USER = 'LOGOUT_USER';
export const LOGOUT_USER_SUCCESS = 'LOGOUT_USER_SUCCESS';

export const loginFacebookSuccess = (data) =>  {
    return {type: LOGIN_FACEBOOK_SUCCESS, data}
};

export const registerFacebook = (data) => {
    return {type: REGISTER_FACEBOOK, data}
};

export const logoutUserSuccess = () => {
    return {type: LOGOUT_USER_SUCCESS}
};

export const logoutUser = () => {
    return {type: LOGOUT_USER}
};
