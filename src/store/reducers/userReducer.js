import {LOGIN_FACEBOOK_SUCCESS, LOGOUT_USER_SUCCESS} from "../actions/usersActions";

const initialState = {
    registerError: null,
    loginError: null,
    user: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_FACEBOOK_SUCCESS:
            return {...state, user: action.data};

        case LOGOUT_USER_SUCCESS:
            return {...state, user: null};
        default:
            return state;
    }
};

export default reducer;