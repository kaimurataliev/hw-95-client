import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import {routerMiddleware, routerReducer} from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import createSagaMiddleware from 'redux-saga'
import rootSaga from './sagas/index';
import {saveState, loadState} from "./localStorage";


import usersReducer from "./reducers/userReducer";


const rootReducer = combineReducers({
    users: usersReducer,
    routing: routerReducer
});

const sagaMiddleware = createSagaMiddleware();


export const history = createHistory();

const middleware = [
    thunkMiddleware,
    routerMiddleware(history),
    sagaMiddleware
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveState({
        users: store.getState().users
    });
});

sagaMiddleware.run(rootSaga);

export default store;