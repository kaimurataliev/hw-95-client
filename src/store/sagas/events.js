import {put , takeEvery, select } from 'redux-saga/effects'
import {FETCH_EVENTS, ADD_EVENT, fetchAllEvents} from '../actions/eventsActions';
import axios from '../../axios';


export function* getEvents(action) {
    try {
        yield put(fetchAllEvents);
    } catch (error) {
        console.log(error);
    }
}

export function* addEvents(action) {
    try {
        yield put(axios.post('/events/addEvent', action.data));
    } catch (error) {
        console.log(error);
    }
}

export function* watchEvents() {
    yield takeEvery(FETCH_EVENTS, getEvents);
    yield takeEvery(ADD_EVENT, addEvents)
}