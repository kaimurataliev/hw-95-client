import {put , takeEvery, select } from 'redux-saga/effects'
import {REGISTER_FACEBOOK, LOGOUT_USER, loginFacebookSuccess, logoutUserSuccess} from '../actions/usersActions';
import axios from '../../axios';

export function* fbRegister(action) {
    try {
        const response = yield axios.post('/users/facebookLogin', action.data);
        yield put(loginFacebookSuccess(response.data))

    } catch (error) {
        console.log(error)
    }
}

export function* fbLogout(action) {
    try {
        yield axios.delete('/users/sessions');
        yield put(logoutUserSuccess())
    } catch (error) {
        console.log(error);
    }
}

export function* watchFacebook() {
    yield takeEvery(REGISTER_FACEBOOK, fbRegister);
    yield takeEvery(LOGOUT_USER, fbLogout)
}