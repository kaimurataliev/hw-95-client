import { all } from 'redux-saga/effects'
import { watchEvents } from './events';
import {watchFacebook} from './facebookLogin'

export default function* rootSaga() {
    yield all([
        watchEvents(),
        watchFacebook()
    ])
}