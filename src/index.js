import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import store from './store/configureStore';
import axios from './axios';

const history = createHistory();

axios.interceptors.request.use(config => {
    try {
        config.headers['Token'] = store.getState().users.user.token;
    } catch (error) {
        console.log(error);
    }

    return config;
});

const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
