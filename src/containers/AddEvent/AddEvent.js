import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, Label} from "react-bootstrap";
import Redirect from "react-router-dom/es/Redirect";
import {addEvent} from "../../store/actions/eventsActions";

class AddEvent extends Component {

    state = {
        author: '',
        startDate: '',
        endDate: '',
        text: '',
        title: ''
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.addEvent(this.state);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value, author: this.props.user._id
        });
    };


    render() {

        if(!this.props.user) {
            return (
                <Redirect to="/"/>
            )
        }

        return (
            <Form horizontal onSubmit={this.submitFormHandler}>

                <Label style={{fontSize: '16px'}} bsStyle="primary">Add new photo</Label>

                <FormGroup controlId="title">
                    <Col componentClass={ControlLabel} sm={2}>
                        Title
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="text"
                            required
                            placeholder='Enter title'
                            name="title"
                            value={this.state.title}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="title">
                    <Col componentClass={ControlLabel} sm={2}>
                        Text
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="text"
                            required
                            placeholder="Enter text"
                            name="text"
                            value={this.state.text}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="title">
                    <Col componentClass={ControlLabel} sm={2}>
                        Start date
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="text"
                            required
                            placeholder="Start date"
                            name="startDate"
                            value={this.state.startDate}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="title">
                    <Col componentClass={ControlLabel} sm={2}>
                        End date
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="text"
                            required
                            placeholder="End date"
                            name="endDate"
                            value={this.state.endDate}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addEvent: (event) => dispatch(addEvent(event))
    }
};

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddEvent)