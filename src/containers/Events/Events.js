import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {FormGroup, Button, Col} from 'react-bootstrap';
import {registerFacebook} from "../../store/actions/usersActions";
import config from '../../config';

import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import {fetchEvents} from "../../store/actions/eventsActions";

class MainPage extends Component {

    // componentDidMount() {
    //     this.props.fetchEvents();
    //     console.log('jbnsfjnvsjlnv');
    // }

    facebookResponse = response => {
        if (response.id) {
            this.props.registerFacebook(response)
        }
    };

    render() {
        return (
            <FormGroup>
                <Col smOffset={2} sm={10}>
                    <FacebookLogin
                        appId={config.facebookAppId}
                        fields="name,email,picture"
                        render={renderProps => (
                            <Button onClick={renderProps.onClick}>fb</Button>
                        )}
                        callback={this.facebookResponse}
                    />
                </Col>
            </FormGroup>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        registerFacebook: (data) => dispatch(registerFacebook(data)),
        fetchEvents: (data) => dispatch(fetchEvents(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);